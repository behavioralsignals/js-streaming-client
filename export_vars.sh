#!/usr/bin/env bash

export CLOUD_PROVIDER=azure   # choices [aws, azure]
export AWS_ACCESS_KEY_ID=`aws configure get aws_access_key_id`
export AWS_SECRET_ACCESS_KEY=`aws configure get aws_secret_access_key`
export AWS_REGION=`aws configure get region`
case "$CLOUD_PROVIDER" in
aws)    echo "Using AWS cloud provider"
        export CONTAINER_REGISTRY="275759788720.dkr.ecr.us-west-2.amazonaws.com"
        ;;
azure)  echo "Using Azure cloud provider"
        export CONTAINER_REGISTRY="behavioralsignals.azurecr.io"
        ;;
*) echo "Cloud provider $1 is not supported"
        return
        ;;
esac

export NCORES=`getconf _NPROCESSORS_ONLN 2>/dev/null || sysctl -n hw.ncpu`
