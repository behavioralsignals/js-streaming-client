/**
 * Functions that enable the streaming client to work with the API
 * @details Includes network negotation with websocket, local audio device handlers,
 *              REST API requests and gui update functions
 * @author Spiros Dimopoulos {sdim@behavioralsignals.com},
 * @author Dimitris Mastrogiannopoulos {dimastro@behavioralsignals.com}
 * @author Giannis Karavasilis {giannis@behavioralsignals.com}
 * @copyright Behavioral Signals Inc
 * @since 1.0
 */

var webSocket;
var messages = document.getElementById("messages");
var arrayBuffer;
var context;
var myTimer;
var bufferUnusedSamples = [];
var localStream;
var sampleRateFinal = 8000;
var apihostname = "";
var clientid = "";
var token = "";
var pid = null;
var microphone = null;
var analyser = null;
var responses_str = "";
var speaker1 = [];
var speaker2 = [];
var drawElem = ["rate1", "rate2"];
var plotly_layout;
var access_mode = "external";
var channels = 2;
var radios_channels = document.formControls.channels;

var canvas = document.querySelector(".visualizer");
var canvasCtx = canvas.getContext("2d");

var drawVisual;

const categories = {
  SPEAKING_RATE: 0,
  TONE_VARIETY: 1
};

const mode = {
  INTERNAL: "internal",
  EXTERNAL: "external"
};

document.getElementById("hostname").addEventListener("keyup", updateHostName);
document.getElementById("clientid").addEventListener("keyup", updateClientId);
document.getElementById("token").addEventListener("keyup", updateToken);
document.getElementById("pid").addEventListener("keyup", function() {
  pid = document.getElementById("pid").value;
});

function getWebSocketURI() {
  return location.protocol === "https:"
    ? "wss://" + apihostname
    : "ws://" + apihostname + ":8080";
}

function getRestInitForMode(connectionMode){
  return getWebURIDomain(connectionMode).concat(getWebURIEndPoint());
}

function getWebURIDomain(connectionMode) {
  return connectionMode == mode.INTERNAL
    ? "http://" + apihostname + ":8080"
    : "https://" + apihostname;
}

function getWebURIEndPoint(){
  return `/client/${clientid}/process/stream?tag=test,stream&channels=${channels}&calldirection=1&storedata=1`;
}

function disableChannels() {
  for (var i = 0, iLen = radios_channels.length; i < iLen; i++) {
    radios_channels[i].disabled = true;
  }
}

function enableChannels() {
  for (var i = 0, iLen = radios_channels.length; i < iLen; i++) {
    radios_channels[i].disabled = false;
  }
}

function initDraw(drawCategory) {
  speaker1[drawCategory] = {
    x: [0],
    y: [0],
    mode: "lines+markers",
    type: "scatter",
    name: "speaker1"
  };

  speaker2[drawCategory] = {
    x: [0],
    y: [0],
    mode: "lines+markers",
    type: "scatter",
    name: "speaker2"
  };

  plotly_layout = {
    width: 500,
    height: 300,
    margin: {
      l: 30,
      r: 30,
      b: 50,
      t: 50
    }
  };

  var data = [speaker1[drawCategory], speaker2[drawCategory]];

  Plotly.newPlot(drawElem[drawCategory], data, plotly_layout);
}

function remapSpeakingRate(rate) {
  switch(rate["value"]) {
    case "very slow":
      return 1;
    case "slow":
      return 2;
    case "normal":
      return 3;
    case "fast":
      return 4;
    case "very fast":
      return 5;
    default:
      return 3;
  }
}

function updateDraw(drawCategory, data) {
  try {
    if (data[0] == ",") {
      data = data.substring(1);
    } else if (data[data.length - 1] == ",") {
      data = data.substring(0, data.length - 1);
    }

    let segment = JSON.parse(data);
    let scalar_val1 = -1;
    let scalar_val2 = -1;
    let timestart = -4;
    let timestop = timestart + 3;
    let tone_variety =
      "tone_variety" in segment
        ? "tone_variety"
        : "vocal_variety" in segment
        ? "vocal_variety"
        : null;
    let speaking_rate =
      "speaking_rate" in segment
        ? "speaking_rate"
        : null;

    if (drawCategory == categories.SPEAKING_RATE && speaking_rate) {
      timestart = segment[speaking_rate].st;
      timestop = segment[speaking_rate].et;
      if ("speaker1" in segment[speaking_rate]) {
        scalar_val1 = remapSpeakingRate(segment[speaking_rate].speaker1);
      }
      if ("speaker2" in segment[speaking_rate]) {
        scalar_val2 = remapSpeakingRate(segment[speaking_rate].speaker2);
      }
    } else if (drawCategory == categories.TONE_VARIETY && tone_variety) {
      timestart = segment[tone_variety].st;
      timestop = segment[tone_variety].et;
      if ("speaker1" in segment[tone_variety]) {
        scalar_val1 = segment[tone_variety].speaker1;
      }
      if ("speaker2" in segment[tone_variety]) {
        scalar_val2 = segment[tone_variety].speaker2;
      }
    } else if (drawCategory == 0) {
      writeResponse(data);
    }

    if (timestart == 0) {
      if (scalar_val1 >= 0) {
        speaker1[drawCategory].x.push(timestart);
        speaker1[drawCategory].y.push(scalar_val1);
      }
      if (scalar_val2 >= 0) {
        speaker2[drawCategory].x.push(timestart);
        speaker2[drawCategory].y.push(scalar_val2);
      }
    } else {
      if (scalar_val1 >= 0) {
        speaker1[drawCategory].x.push(timestop);
        speaker1[drawCategory].y.push(scalar_val1);
      }
      if (scalar_val2 >= 0) {
        speaker2[drawCategory].x.push(timestop);
        speaker2[drawCategory].y.push(scalar_val2);
      }
    }
    var dataplot = [speaker1[drawCategory], speaker2[drawCategory]];

    Plotly.newPlot(drawElem[drawCategory], dataplot, plotly_layout);
    Plotly.relayout(drawElem[drawCategory], {
      "xaxis.autorange": true,
      "yaxis.autorange": true
    });
  } catch (e) {
    console.error(
      "Syntax Error: ",
      e instanceof SyntaxError,
      "\nMessage: ",
      e.message,
      "\nName: ",
      e.name
    );
  }
}

function updateHostName() {
  apihostname = document.getElementById("hostname").value;
}

function updateClientId() {
  clientid = document.getElementById("clientid").value;
}

function updateToken() {
  token = document.getElementById("token").value;
}

function updateMode(newmode) {
  access_mode = newmode;
}

function updateChannels(newchannels) {
  channels = newchannels;
}

function openSocket() {
  // Ensures only one connection is open at a time
  writeResponse("Opening websocket connection...");
  updateHostName();
  updateClientId();
  updateToken();

  if (webSocket !== undefined && webSocket.readyState !== WebSocket.CLOSED) {
    writeResponse("WebSocket is already opened.");
    return;
  }

  // Create a new instance of the websocket
  let apiUrlDomain = getWebSocketURI();
  webSocket = new WebSocket(
    apiUrlDomain + "/stream/" + clientid + "?X-Auth-Token=" + token
  );

  // Binds functions to the listeners for the websocket.
  webSocket.onopen = function(event) {
    // For reasons I can't determine, onopen gets called twice
    // and the first time event.data is undefined.
    if (event.data === undefined) {
      return;
    }
    writeResponse(event.data);
  };

  webSocket.onmessage = function(event) {
    updateDraw(categories.SPEAKING_RATE, event.data);
    updateDraw(categories.TONE_VARIETY, event.data);
  };

  webSocket.onclose = function(event) {
    writeResponse("Connection closed");
    if (context) {
      context.close();
    }
  };
}

function sendStart() {
  writeResponse("Sending start message to WS for pid " + pid);
  webSocket.send(
    JSON.stringify({
      control: "start",
      pid: pid,
      channels: channels.toString()
    })
  );
  speaker1 = [];
  speaker2 = [];
  initDraw(categories.SPEAKING_RATE);
  initDraw(categories.TONE_VARIETY);
}

function sendStop() {
  writeResponse("Sending stop message to WS");
  webSocket.send(JSON.stringify({ control: "end" }));
  setPID("");
  enableChannels();
}

/**
 * Sends the value of the arrayBuffer to Server
 */
function sleep(ms) {
  return new Promise(resolve => setTimeout(resolve, ms));
}

async function send() {
  let csize = 8000;
  if (arrayBuffer) {
    for (let counter = 0; counter < arrayBuffer.byteLength; counter += csize) {
      if (counter % (channels * 2 * csize) == 0) {
        writeResponse(
          "Simulated send of " +
            counter / (channels * 2 * csize) +
            " seconds of live audio"
        );
      }
      if (counter + csize <= arrayBuffer.byteLength) {
        webSocket.send(arrayBuffer.slice(counter, counter + csize));
      } else {
        webSocket.send(arrayBuffer.slice(counter, arrayBuffer.byteLength));
      }
      await sleep(250);
    }
    writeResponse("File sent to server");
  } else {
    writeResponse("Error: No file selected!");
  }
}

function closeSocket() {
  webSocket.close();
  enableChannels();
  setPID("");
}

function writeResponse(text) {
  if (responses_str.length > 1024) {
    responses_str = responses_str.substr(responses_str.length - 1 - 1024, 1024);
  }
  responses_str += "<br/>" + text;
  messages.innerHTML = responses_str;
  messages.scrollTop = messages.scrollHeight;
}

document.querySelector("#fileinput").addEventListener(
  "change",
  function() {
    var reader = new FileReader();
    reader.onload = function() {
      arrayBuffer = this.result;
    };
    reader.readAsArrayBuffer(this.files[0]);
  },
  false
);

function setPID(newpid) {
  var pidtxt = document.getElementById("pid");
  pidtxt.value = newpid;
}

function submitTCP() {
  writeResponse("Requesting PID for live demo...");
  setPID("");

  restUrl = getRestInitForMode(access_mode);

  const options = {
    method: "POST",
    headers: {
      "X-Auth-Token": token
    },
    cache: "no-cache"
  };
  fetch(restUrl, options)
    .then(data => data.json())
    .then(response => {
      if (response) {
        pid = response.pid;
        if (pid > 0) {
          setPID(pid);
          disableChannels();
          writeResponse("Init successful with pid " + pid);
        } else {
          writeResponse("Could not retrieve Process ID from API!");
        }
      } else {
        writeResponse("Could not read API response!");
      }
    })
    .catch(error => {
      console.error("Error on POST request. Error: ", error);
    });
  return false;
}

function recaudio() {
  writeResponse("Start capturing mic audio and sending to callER...");
  if (webSocket) {
    var promisifiedOldGUM = function(constraints) {
      // First get ahold of getUserMedia, if present
      var getUserMedia =
        navigator.getUserMedia ||
        navigator.webkitGetUserMedia ||
        navigator.mozGetUserMedia;

      // Some browsers just don't implement it - return a rejected promise with an error
      // to keep a consistent interface
      if (!getUserMedia) {
        return Promise.reject(
          new Error("getUserMedia is not implemented in this browser")
        );
      }

      // Otherwise, wrap the call to the old navigator.getUserMedia with a Promise
      return new Promise(function(resolve, reject) {
        getUserMedia.call(navigator, constraints, resolve, reject);
      });
    }; //promisifiedOldGUM

    // Older browsers might not implement mediaDevices at all, so we set an empty object first
    if (navigator.mediaDevices === undefined) {
      navigator.mediaDevices = {};
    }

    // Some browsers partially implement mediaDevices. We can't just assign an object
    // with getUserMedia as it would overwrite existing properties.
    // Here, we will just add the getUserMedia property if it's missing.
    if (navigator.mediaDevices.getUserMedia === undefined) {
      navigator.mediaDevices.getUserMedia = promisifiedOldGUM;
    }

    // Prefer camera resolution nearest to 1280x720.
    //var constraints = { audio: true, video: { width: 1280, height: 720 } };
    var constraints = { audio: true, video: false };

    navigator.mediaDevices
      .getUserMedia(constraints)
      .then(function(stream) {
        context = new (window.AudioContext || window.webkitAudioContext)(); // define audio context
        sampleRate = context.sampleRate;
        microphone = context.createMediaStreamSource(stream);
        analyser = context.createAnalyser();
        //var filter = context.createBiquadFilter();

        var bufferSize = 16384;
        // record only 1 channel
        var livefeed = context.createScriptProcessor(bufferSize, 1, 1);
        // specify the processing function
        livefeed.onaudioprocess = livefeedProcess;
        // connect stream to our livefeed
        microphone.connect(livefeed);

        microphone.connect(analyser);
        analyser.fftSize = 2048;
        var bufferLength = analyser.frequencyBinCount;
        var dataArray = new Uint8Array(bufferLength);
        WIDTH = canvas.width;
        HEIGHT = canvas.height;
        canvasCtx.clearRect(0, 0, WIDTH, HEIGHT);

        function draw() {
          drawVisual = requestAnimationFrame(draw);
          analyser.getByteTimeDomainData(dataArray);
          canvasCtx.fillStyle = "rgb(200, 200, 200)";
          canvasCtx.fillRect(0, 0, WIDTH, HEIGHT);
          canvasCtx.lineWidth = 2;
          canvasCtx.strokeStyle = "rgb(0, 0, 0)";

          canvasCtx.beginPath();
          var sliceWidth = (WIDTH * 1.0) / bufferLength;
          var x = 0;
          for (var i = 0; i < bufferLength; i++) {
            var v = dataArray[i] / 128.0;
            var y = (v * HEIGHT) / 2;

            if (i === 0) {
              canvasCtx.moveTo(x, y);
            } else {
              canvasCtx.lineTo(x, y);
            }

            x += sliceWidth;
          }
          canvasCtx.lineTo(canvas.width, canvas.height / 2);
          canvasCtx.stroke();
        }
        draw();
        // connect our liveffed to the previous destination
        livefeed.connect(context.destination);

        sendHeader(sampleRateFinal, 4000000);
        writeResponse("Sending wav header...");
        // microphone -> filter -> destination.
        //  microphone.connect(context.destination);
        //filter.connect(context.destination);
        //  var audio = document.getElementById("gum-local");
        // audio.src = window.URL.createObjectURL(stream);
        //audio.onloadedmetadata = function(e) {
        //  audio.play();
        //};
        writeResponse("Sending raw audio from mic...");
        localStream = stream;
        document.getElementById("startrec").disabled = true;
        document.getElementById("stoprec0").disabled = false;
        $(".visualizer").css("visibility", "visible");
      })
      .catch(function(err) {
        console.error(err.name + ": " + err.message);
      });
  } else {
    writeResponse("Socket not connected yet.");
  }
}

function stoprec() {
  if (microphone != null) microphone.disconnect();
  if (analyser != null) analyser.disconnect();
  if (context != null) context.close();
  document.getElementById("startrec").disabled = false;
  document.getElementById("stoprec0").disabled = true;
  $(".visualizer").css("visibility", "hidden");
}

function livefeedProcess(e) {
  if (e) {
    var left = e.inputBuffer.getChannelData(0);
    left_downsampled = downsample(left, sampleRate, 8000);
    webSocket.send(floatTo16BitPCM(left_downsampled));
  } else {
    writeResponse("Error: No buffer data selected!");
  }
}

function convertFloat32ToInt16(buffer) {
  l = buffer.length;
  buf = new Int16Array(l);
  while (l--) {
    buf[l] = Math.min(1, buffer[l]) * 0x7fff;
  }
  return buf.buffer;
}

function writeUTFBytes(view, offset, string) {
  var lng = string.length;
  for (var i = 0; i < lng; i++) {
    view.setUint8(offset + i, string.charCodeAt(i));
  }
}

function sendHeader(sampleRate, length) {
  // create the buffer and view to create the .WAV file
  var buffer = new ArrayBuffer(44);
  var view = new DataView(buffer);
  // write the WAV container, check spec at: https://ccrma.stanford.edu/courses/422/projects/WaveFormat/
  // RIFF chunk descriptor
  writeUTFBytes(view, 0, "RIFF");
  view.setUint32(4, 44 + length * 2, true);
  writeUTFBytes(view, 8, "WAVE");
  // FMT sub-chunk
  writeUTFBytes(view, 12, "fmt ");
  view.setUint32(16, 16, true);
  view.setUint16(20, 1, true);
  // mono (1 channels)
  view.setUint16(22, 1, true);
  view.setUint32(24, sampleRate, true);
  view.setUint32(28, sampleRate * 4, true);
  view.setUint16(32, 4, true);
  view.setUint16(34, 16, true);
  // data sub-chunk
  writeUTFBytes(view, 36, "data");
  view.setUint32(40, length * 2, true);

  webSocket.send(buffer);
}

function results() {
  var pid = document.getElementById("pid").value;
  myTimer = setInterval(function() {
    pollFunc(pid);
  }, 3000);
}

/**
 * Downsamples WebAudio to 16 kHz.
 *
 * Browsers can downsample WebAudio natively with OfflineAudioContext's but it was designed for non-streaming use and
 * requires a new context for each AudioBuffer. Firefox can handle this, but chrome (v47) crashes after a few minutes.
 * So, we'll do it in JS for now.
 *
 * This really belongs in it's own stream, but there's no way to create new AudioBuffer instances from JS, so its
 * fairly coupled to the wav conversion code.
 *
 * @param  {AudioBuffer} buffer Microphone/MediaElement audio chunk
 * @return {Float32Array} 'audio/l16' chunk
 */
downsample = function downsample(
  bufferNewSamples,
  sourceSampleRate,
  targetSampleRate
) {
  var buffer = null,
    newSamples = bufferNewSamples.length,
    unusedSamples = bufferUnusedSamples.length;
  if (unusedSamples > 0) {
    buffer = new Float32Array(unusedSamples + newSamples);
    for (var i = 0; i < unusedSamples; ++i) {
      buffer[i] = this.bufferUnusedSamples[i];
    }
    for (i = 0; i < newSamples; ++i) {
      buffer[unusedSamples + i] = bufferNewSamples[i];
    }
  } else {
    buffer = bufferNewSamples;
  }
  // downsampling variables
  /*var filter = [
      -0.037935, -0.00089024, 0.040173, 0.019989, 0.0047792, -0.058675, -0.056487,
      -0.0040653, 0.14527, 0.26927, 0.33913, 0.26927, 0.14527, -0.0040653, -0.056487,
      -0.058675, 0.0047792, 0.019989, 0.040173, -0.00089024, -0.037935
    ],*/
  var filter = [
      0.0015,
      0.0007,
      -0.0012,
      -0.0052,
      -0.0109,
      -0.0152,
      -0.0133,
      0.0003,
      0.0283,
      0.0688,
      0.1144,
      0.1543,
      0.1776,
      0.1776,
      0.1543,
      0.1144,
      0.0688,
      0.0283,
      0.0003,
      -0.0133,
      -0.0152,
      -0.0109,
      -0.0052,
      -0.0012,
      0.0007,
      0.0015
    ],
    samplingRateRatio = sourceSampleRate / targetSampleRate,
    nOutputSamples =
      Math.floor((buffer.length - filter.length) / samplingRateRatio) + 1,
    outputBuffer = new Float32Array(nOutputSamples);
  for (
    var offset, i2 = 0;
    Math.round(samplingRateRatio * i2) + filter.length - 1 < buffer.length;
    i2++
  ) {
    offset = Math.round(samplingRateRatio * i2);
    var sample = 0;
    for (var j = 0; j < filter.length; ++j) {
      sample += buffer[offset + j] * filter[j];
    }
    outputBuffer[i2] = sample;
  }
  var indexSampleAfterLastUsed = Math.round(samplingRateRatio * i2);
  var remaining = buffer.length - indexSampleAfterLastUsed;
  if (remaining > 0) {
    bufferUnusedSamples = new Float32Array(remaining);
    for (i = 0; i < remaining; ++i) {
      bufferUnusedSamples[i] = buffer[indexSampleAfterLastUsed + i];
    }
  } else {
    bufferUnusedSamples = new Float32Array(0);
  }
  return outputBuffer;
};

/**
 * Accepts a Float32Array of audio data and converts it to a Buffer of l16 audio data (raw wav)
 *
 * Explanation for the math: The raw values captured from the Web Audio API are
 * in 32-bit Floating Point, between -1 and 1 (per the specification).
 * The values for 16-bit PCM range between -32768 and +32767 (16-bit signed integer).
 * Filter & combine samples to reduce frequency, then multiply to by 0x7FFF (32767) to convert.
 * Store in little endian.
 *
 * @param input
 * @returns {Buffer}
 */
floatTo16BitPCM = function(input) {
  var output = new DataView(new ArrayBuffer(input.length * 2)); // length is in bytes (8-bit), so *2 to get 16-bit length
  for (var i = 0; i < input.length; i++) {
    var multiplier = input[i] < 0 ? 0x8000 : 0x7fff; // 16-bit signed range is -32768 to 32767
    output.setInt16(i * 2, (input[i] * multiplier) | 0, true); // index, value, little edian
  }
  return output.buffer;
};
