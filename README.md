# js-streaming-client

This repository contains source code of a simple JS client for the Streaming API
The client can be used for demostration purposes or an example of the API usage

To use it you can clone it and run it using a web server

How to use

* Open in a browser: [https://apps.behavioralsignals.com/frontend/js-example-client/](https://apps.behavioralsignals.com/frontend/js-example-client/)
* API host: live.behavioralsignals.com
* Account ID: [ask admin]
* Token: [ask admin]
* Press Open WS
* Press REST Init
* Press Start Task
* Choose record Mic (allow Mic access to browser tab)
* Start talking. You should see it live with plots updated every 3 sec
* Press Stop (next to record mic)
* Press End Task
* Press Close WS

Copyright: _Behavioral Signals Inc_
Contact: _sdim@behavioralsignals.com_
